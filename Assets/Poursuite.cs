using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poursuite : MonoBehaviour
{
    public GameObject Cible;
    public float vitesse = 0.1f;

    void Update()
    {

        Vector3 C = this.transform.position;

        Vector3 S = Cible.transform.position;

        Vector3 direction = (S - C).normalized;

        this.transform.position += direction * vitesse * Time.deltaTime;
    }
}
